
<?php
header('Content-Type: application/xml');
function connect() {
    return new PDO('mysql:host=localhost;dbname=usa', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}

$pdo = connect();

// posts *******************************
$sql = 'SELECT * FROM news ORDER BY id DESC';
$query = $pdo->prepare($sql);
$query->execute();
$rs_post = $query->fetchAll();

// The XML structure
$data = '<?xml version="1.0" encoding="UTF-8" ?>';
$data .= '<rss version="2.0">';
$data .= '<channel>';
$data .= '<title>Bewebdeveloper : Free web tutorials</title>';
$data .= '<link>http://www.bewebdeveloper.com</link>';
$data .= '<description>Free Web tutorials with source code, PHP Tutorials, JavaScript Tutorials, HTML Tutorials and CSS Tutorials</description>';
foreach ($rs_post as $row) {
    $data .= '<item>';
    $data .= '<title>'.$row['en_thema'].'</title>';
    $data .= '<link>'.$row['date'].'</link>';
    $data .= '<description>'.$row['en_description'].'</description>';
    $data .= '</item>';
}
$data .= '</channel>';
$data .= '</rss> ';


echo $data;
?>