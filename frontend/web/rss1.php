<?php
$mysql_host = 'localhost'; //host
$mysql_username = 'root'; //username
$mysql_password = 'Mf@2018urikt!'; //password
$mysql_database = 'adminmfa'; //db

header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML

$rss = new SimpleXMLElement('<rss xmlns:dc="http://uzbekistan.org" xmlns:content="http://uzbekistan.org/rss.php" xmlns:atom="http://uzbekistan.org"></rss>');
$rss->addAttribute('version', '2.0');

// $channel = $rss->addChild('channel'); //add channel node

// $atom = $rss->addChild('atom:atom:link'); //add atom node
// $atom->addAttribute('href', 'http://localhost'); //add atom node attribute
// $atom->addAttribute('rel', 'self');
// $atom->addAttribute('type', 'application/rss+xml');

// $title = $rss->addChild('title','Sanwebe'); //title of the feed
// $description = $rss->addChild('description','description line goes here'); //feed description
// $link = $rss->addChild('link','http://www.sanwebe.com'); //feed site
// $language = $rss->addChild('language','en-us'); //language

// //Create RFC822 Date format to comply with RFC822
// $date_f = date("D, d M Y H:i:s T", time());
// $build_date = gmdate(DATE_RFC2822, strtotime($date_f)); 
// $lastBuildDate = $rss->addChild('lastBuildDate',$date_f); //feed last build date

// $generator = $rss->addChild('generator','PHP Simple XML'); //add generator node


//connect to MySQL - mysqli(HOST, USERNAME, PASSWORD, DATABASE);
$mysqli = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);

//Output any connection error
if ($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}
$results = $mysqli->query("SELECT * FROM news ORDER BY id DESC");

if($results){ //we have records 
    while($row = $results->fetch_object()) //loop through each row
    {
        $item = $rss->addChild('item'); //add item node
        $title = $item->addChild('title', $row->en_thema); //add title node under item
        $link = $item->addChild('link', 'http://uzbekistan.org/news/view?id='. $row->id); //add link node under item
        // $guid = $item->addChild('guid', 'http://uzbekistan.org/news/view?id='. $row->id); //add guid node under item
        // $guid->addAttribute('isPermaLink', 'false'); //add guid node attribute
        
        $description = $item->addChild('description', '<![CDATA['. htmlentities($row->en_description) . ']]>'); //add description
        
        //$date_rfc = gmdate(DATE_RFC2822, strtotime($row->published));
        $item = $item->addChild('pubDate', $row->date); //add pubDate node
    }
}

echo $rss->asXML(); //output XML