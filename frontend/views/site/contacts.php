<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;


?>
<div class="main-content shop-page contact-page">
    <div class="container">
        <div class="breadcrumbs">
            <a href="#"><?= Yii::t('app', 'Asosiy') ?></a> \ <span class="current"><?= Yii::t('app', 'Biz bilan aloqa') ?></span>
        </div>
        <div class="row content-form ">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 map-content">
                <div class="map">
                    <a href="#">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11987.182030758127!2d69.27930974288333!3d41.31331203266396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae8b25f9d2446b%3A0xe2b85418577cf1a6!2sPoytaxt%20savdo%20markazi!5e0!3m2!1suz!2s!4v1592817309294!5m2!1suz!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </a>
                    </div>
                <div class="information-form">
                    <!-- <h4 class=" main-title">Our Store</h4> -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <!-- <h5 class="title">Store 1</h5> -->
                            <ul class="list-info">
                                <li>
                                    <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                                    <div class="info">
                                        <h5 class="subtitle"><?= Yii::t('app', 'Email') ?></h5>
                                        <a href="#" class="des">Support@jazd.com</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                    <div class="info">
                                        <h5 class="subtitle"><?= Yii::t('app', 'Phone') ?></h5>
                                        <p class="des">(+99891)2222222</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                    <div class="info">
                                        <h5 class="subtitle"><?= Yii::t('app', 'Office') ?></h5>
                                        <p class="des"><?= Yii::t('app', 'Mazilimiz') ?></p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                       <!--  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <h5 class="title">Hours Of Operation</h5>
                            <ul class="time-work">
                                <li><div class="day">Monday:</div><div class="time">12-5 PM</div></li>
                                <li><div class="day">Tuesday:</div><div class="time">12-5 PM</div></li>
                                <li><div class="day">Wendnesday:</div><div class="time">12-5 PM</div></li>
                                <li><div class="day">Thursday:</div><div class="time">12-5 PM</div></li>
                                <li><div class="day">Friday:</div><div class="time">12-5 PM</div></li>
                                <li><div class="day">Saturday:</div><div class="time">12-5 PM</div></li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 info-content">
                <div class="contact-form">
                    <h4 class="main-title"><?= Yii::t('app', 'Biz bilan aloqa') ?></h4>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!-- <span class="label-text">Your Name *</span> -->
                            <label><?= Yii::t('app', 'name') ?></label>
                            <?php  echo $form->field($model, 'name')->textInput(['autofocus' => true])->label(false) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!-- <span class="label-text">Email Address *</span> -->
                            <label><?= Yii::t('app', 'email') ?></label>
                            <?= $form->field($model, 'email')->label(false) ?>
                        </div>
                        <!-- </div> -->
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!-- <span class="label-text">Subject</span> -->
                            <label><?= Yii::t('app', 'subject') ?></label>
                            <?= $form->field($model, 'subject')->label(false) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <!-- <span class="label-text">Phone Number</span> -->
                            <label><?= Yii::t('app', 'Phone') ?></label>
                            <?= $form->field($model, 'phone')->label(false) ?>
                        </div>
                    </div>
                    <!-- <span class="label-text">Your Message *</span> -->
                    <label><?= Yii::t('app', 'body') ?></label>
                    <?= $form->field($model, 'body')->textarea(['rows' => 6])->label(false) ?>
                    <label><?= Yii::t('app', 'verifyCode') ?></label>
                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ])->label(false) ?>
                    <div class="form-group">
                        <?= Html::submitButton('Yuborish', ['class' => 'btn btn-danger', 'name' => 'contact-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
    </div>
</div>
