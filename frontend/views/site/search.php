 <?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use backend\models\Menu;
use backend\models\News;
use backend\models\Page;
use backend\models\SubMenu;
use backend\models\TopMenu;

$menus = Menu::find()->all();
$topmenus = TopMenu::find()->all();

$lang = Yii::$app->language;


$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page animsition" style="top: 40px;">
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-9">
                        	<form id="searchForm" action="<?= Url::to(['/site/search']) ?>" method="get">
                                            <div class="input-group">
                                                 <?php // $form = ActiveForm::begin(); ?>
                                                <input type="text" class="form-control" name="q" id="q"  placeholder="Search..."  value="<?=Html::encode(\yii\helpers\HtmlPurifier::process(Yii::$app->request->get('q')))?>" required>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                                </span>
                                                <?php // ActiveForm::end(); ?>
                                            </div>
                                        </form><br>
                            <div class="blog-posts">
                            	<h4>Siz qidirgan ma'lumotlar</h4><hr>
                            	<?php 	

                            	  $i = 0;
								    $stop = false;
								    foreach ($result as $sectionKey => $sectionValue) {
								        if ($stop) {
								            break;
								        }
								        if (is_array($sectionValue)) {
								            foreach ($sectionValue as $item) {
								                if ($sectionKey == 'news') {
								                    $link = Html::a($item[$lang.'_thema'], ['/news/view', 'id' => $item['id']]);
								                    $content = strip_tags($item[$lang.'_description']);
								                } elseif ($sectionKey == 'page') {
								                    $link = Html::a(strip_tags($item[$lang.'_name']), ['/page/view', 'id' => $item['id']]);
								                    $content = substr(strip_tags($item[$lang.'_text']), 0, 600) . "...";
								                }

								                echo "<div>
								                    <p>$link</p>
								                    <div>
								                    {$content}
								                    </div>
								                    </div><hr>";

								                $i++;
								                if ($i == 25) {
								                    $stop = TRUE;
								                    break;
								                }
								            }
								        }
								    }


                            	?>
                                
                               
                            </div>
                        </div>

                       <div class="col-md-3">
                          <!-- Page Widget -->
                          <div class="widget widget-shadow text-center">
                            <div class="widget-header">
                              <div class="widget-header-content">
                                    <?php foreach ($services as $key => $service):?>
                                        <div class="col-lg-12 col-md-12" >
                                          <div class="widget widget-shadow text-center">
                                            <div class="widget-header cover overlay" style="height: calc(100% - 100px);">
                                              <img class="cover-image" src="/images/ppp.jpg" alt="..." style="height: 100%;">
                                              <div class="overlay-panel vertical-align">
                                                <div class="vertical-align-middle">
                                                  <a class="avatar avatar-100 bg-white margin-bottom-10 margin-xs-0 img-bordered" <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> style="width: 214px;border-radius: 0px;">
                                                    <img src="/uploads/<?= $service->icon;?>" alt="" style="border-radius: 0px;">
                                                  </a>
                                                  <div class="font-size-15" style="color: #0074b4"><?= $service->{$lang.'_name'};?></div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    <?php endforeach;?>
                                
                              </div>
                            </div>
                          </div>
                          <!-- End Page Widget -->
                        </div>
                    </div>

                </div>

</div>





</div>





  