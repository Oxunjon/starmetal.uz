<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use backend\models\Menu;
use backend\models\SubMenu;
use backend\models\Symbol;
use backend\models\TopMenu;
use backend\models\Chapter;
use backend\models\Article;


use yii\helpers\Url;

$menus = Menu::find()->andWhere(['active'=>[1]])->all();
$symbol= Symbol::find()->all();
$topmenus = TopMenu::find()->all();
$lang = Yii::$app->language;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;

?>



<div class="page animsition" style="top: 40px;">
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-md-9">
          <!-- Panel -->
          <div class="panel">
            <div class="panel-body nav-tabs-animate nav-tabs-horizontal">
              <div class="tab-content">
                <!-- <div class="tab-pane active animation-slide-left" id="activities" role="tabpanel">1111 -->
                  <center>
                    <img alt="uzbekistan.org"   src="/images/kons_<?= $lang?>.jpg" class="img-responsive" >
                  </center><br>
                  <h4 style="text-align: center;">
                            <?= Yii::t('app', 'muqaddima') ?>
                        </h4>
                        <p style="text-align: justify; font-family: Arial; font-size: 16px;">
                            <?= Yii::t('app', 'muqaddima_text') ?>
                        </p>
                        <!-- Example Continuous Accordion -->
                        <div class="examle-wrap">
                          <div class="example">
                            <div class="panel-group panel-group-continuous" id="exampleAccordionContinuous"
                            aria-multiselectable="true" role="tablist">

                              <?php $i=0;?>
                                <?php $a = 0;?>
                                <?php foreach ($sections as $key => $section):?>
                                    <?php $i++;?>
                              <div class="panel1" >
                                <div class="panel-heading" id="exampleHeadingContinuous<?= $i;?>" role="tab">
                                  <a class="panel-title" data-parent="#exampleAccordionContinuous" data-toggle="collapse"
                                  href="#exampleCollapseContinuousa<?= $i;?>" aria-controls="exampleCollapseContinuous<?= $i;?>"
                                  aria-expanded="false" style="color:#3A469D; ">
                                  <?= $section[$lang.'_section'];?>
                                </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapseContinuousa<?= $i;?>" aria-labelledby="exampleHeadingContinuous<?= $i;?>"
                                role="tabpanel">
                                  <div class="panel-body">
                                    <?php $chapters = Chapter::find()->where(['section_id' => [$section->id]])->all(); ?>
                                                 <?php if ($chapters):?>
                                                    <?php foreach ($chapters as $key => $chapter): ?>
                                                        <?php $a++;?>
                                                        <div class="panel">
                                                            <div class="panel-heading" id="exampleHeadingContinuous<?=$a;?>" role="tab">
                                                              <a style="color:#3A469D;" class="panel-title collapsed" data-parent="#exampleAccordionContinuous" data-toggle="collapse"
                                                              href="#exampleCollapseContinuous<?=$a;?>" aria-controls="exampleCollapseContinuous<?=$a;?>"
                                                              aria-expanded="false">
                                                              <?= $chapter[$lang.'_chapter'];?>
                                                            </a>
                                                            </div>
                                                            <div class="panel-collapse collapse" id="exampleCollapseContinuous<?=$a;?>" aria-labelledby="exampleHeadingContinuous<?=$a;?>"
                                                            role="tabpanel">
                                                              <div class="panel-body">
                                                                    <?php $articles = Article::find()->where(['chapter_id' => [$chapter->id]])->all(); ?>
                                                            <?php if ($articles):?>
                                                                <?php foreach ($articles as $key => $article): ?>
                                                                    <a href="#" data-toggle="modal" data-target="#largeModal<?= $article->id;?>" >
                                                                        <i class="icon fa-search-plus" aria-hidden="true"></i> <?= $article[$lang.'_article'];?>
                                                                    </a><br><br>
                                                                    <div class="modal fade" id="largeModal<?= $article->id;?>" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                <h4 class="modal-title" id="largeModalLabel"><?= $article[$lang.'_article'];?></h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <?= $article[$lang.'_article_tex'];?>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                  <?php endforeach;?>
                                                            <?php endif;?>

                                                              </div>
                                                            </div>
                                                          </div>
                                  <?php endforeach;?>
                                    <?php endif;?>  

                                  </div>
                                </div>
                              </div>
                          <?php endforeach;?>

                            </div>
                          </div>
                        </div>
            <!-- End Example Continuous Accordion -->
                <!-- </div> -->
              </div>
            </div>
          </div>
          <!-- End Panel -->
        </div>
        <div class="col-md-3">
          <!-- Page Widget -->
          <div class="widget widget-shadow text-center">
            <div class="widget-header">
              <div class="widget-header-content">
                    <?php foreach ($services as $key => $service):?>
                        <div class="col-lg-12 col-md-12" >
                          <div class="widget widget-shadow text-center">
                            <div class="widget-header cover overlay" style="height: calc(100% - 100px);">
                              <img class="cover-image" src="/images/ppp.jpg" alt="..." style="height: 100%;">
                              <div class="overlay-panel vertical-align">
                                <div class="vertical-align-middle">
                                  <a class="avatar avatar-100 bg-white margin-bottom-10 margin-xs-0 img-bordered" <?php if($service->page!=NULL):?> href="<?= Url::to(['/page/view', 'id' => $service->page  ])?>"<?php endif;?> href="<?= $service->link;?>" <?php if($service->target==1):?> target="_blank"<?php endif;?> style="width: 214px;border-radius: 0px;">
                                    <img src="<?= $service->icon;?>" alt="" style="border-radius: 0px;">
                                  </a>
                                  <div class="font-size-15" style="color: #0074b4"><?= $service->{$lang.'_name'};?></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    <?php endforeach;?>
                
              </div>
            </div>
          </div>
          <!-- End Page Widget -->
        </div>
      </div>
    </div>
  </div>