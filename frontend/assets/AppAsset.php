<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //<!-- Theme CSS -->

        'css/bootstrap.min.css',
        'css/owl.carousel.min.css',
        'css/font-awesome.min.css',
        'css/animate.min.css',
        //'css/magnific-popup.min.css',
        'css/jquery-ui.min.css',
        'css/jquery.scrollbar.min.css',
        'css/chosen.min.css',
        'css/ovic-mobile-menu.css',
        'css/style.css',
        'css/customs-css2.css',



    ];
    public $js = [
      'js/jquery-2.1.4.min.js',
      'js/bootstrap.min.js',
      'js/owl.carousel.min.js',
      'js/owl.thumbs.min.js',
      //'js/magnific-popup.min.js',
      'js/ovic-mobile-menu.js',
      'js/mobilemenu.min.js',
      'js/jquery.plugin-countdown.min.js',
      'js/jquery-countdown.min.js',
      'js/jquery-ui.min.js',
      'js/jquery.scrollbar.min.js',
      'js/chosen.min.js',
      'js/frontend.js',


    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',

    ];
}
