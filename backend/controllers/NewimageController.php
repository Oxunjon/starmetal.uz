<?php

namespace backend\controllers;

use Yii;
use app\models\Newimage;
use backend\models\NewimageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewimageController implements the CRUD actions for Newimage model.
 */
class NewimageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Newimage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewimageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Newimage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Newimage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Newimage();

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $image2 = UploadedFile::getInstance($model, 'image2');
            if (!empty($image)) {
                $model->img = random_int(0,9999). '.' . $image->extension;
            }
             if (!empty($image2)) {
                $model->img2 = random_int(0,9999). '.' . $image2->extension;
            }
            
            if ($model->save()) {
                if (!empty($image)) {
                    $image->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img);
                    // return $this->redirect(['index']);
                }
                if (!empty($image2)) {
                    $image2->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img2);
                    // return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Newimage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

         if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $image2 = UploadedFile::getInstance($model, 'image2');
            if (!empty($image)) {
                $model->img = random_int(0,9999). '.' . $image->extension;
            }
             if (!empty($image2)) {
                $model->img2 = random_int(0,9999). '.' . $image2->extension;
            }
            
            if ($model->save()) {
                if (!empty($image)) {
                    $image->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img);
                    // return $this->redirect(['index']);
                }
                if (!empty($image2)) {
                    $image2->saveAs(Yii::getAlias('@frontend/web/uploads/') . $model->img2);
                    // return $this->redirect(['index']);
                }
                return $this->redirect(['index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Newimage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $data = Newimage::findOne($id);
        unlink(Yii::getAlias('@frontend/web/uploads/') . $data->img);
        unlink(Yii::getAlias('@frontend/web/uploads/') . $data->img2);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Newimage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Newimage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Newimage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
