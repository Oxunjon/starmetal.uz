<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use common\widgets\Alert;

?>
<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">



					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">

							<ul class="navigation navigation-main navigation-accordion">
								<!-- Main -->
								<li class="navigation-header"><span style="text-align: center; font-size: 12px; border: 1px solid lightgreen;"><?= Yii::$app->user->identity->name?></span> <i class="icon-menu" title="Main pages"></i></li>

								<li><a href="<?= Url::to(['/']) ?>"><i class="icon-home4"></i> <span>Главная</span></a></li>
								<li><a href="<?= Url::to(['/energy']) ?>"><i class="icon-magazine"></i> <span>Energy</span></a></li>
								<li><a href="<?= Url::to(['/sayt']) ?>"><i class="icon-magazine"></i> <span>Сайт</span></a></li>
								<li><a href="<?= Url::to(['/car']) ?>"><i class="icon-magazine"></i> <span>Car</span></a></li>
								<li><a href="<?= Url::to(['/news']) ?>"><i class="icon-magazine"></i> <span>Новости</span></a></li>
								<?php if(Yii::$app->user->identity->role_id == '1'):?>
									<li><a href="<?= Url::to(['/categor-news']) ?>"><i class="icon-pencil5"></i> <span>Категория</span></a></li>
								<?php endif;?>
								<li><a href="<?= Url::to(['/page']) ?>"><i class="icon-file-plus"></i> <span>Cтраница</span></a></li>
								<?php if(Yii::$app->user->identity->role_id == '1'):?>
									<li><a href="<?= Url::to(['/symbol']) ?>"><i class="icon-pilcrow"></i> <span>Символ</span></a></li>
								<?php endif;?>
								<li>
									<a href="#"><i class="icon-menu3"></i> <span>Меню</span></a>
									<ul>
										<li><a href="<?= Url::to(['/menu']) ?>"><i class="icon-menu3"></i> <span>Меню</span></a></li>
										<li><a href="<?= Url::to(['/sub-menu']) ?>"><i class="icon-menu5"></i> <span>Суб-меню</span></a></li>
									</ul>
								</li>
								<li><a href="<?= Url::to(['/online']) ?>"><i class="icon-envelop3"></i> <span>Онлайн-приемная</span></a></li>
								<li>
									<a href="#"><i class="icon-flag4"></i> <span>Услуга</span></a>
									<ul>
										<li><a href="<?= Url::to(['/links']) ?>"><i class="icon-hyperlink"></i> <span>Полезные ссылки</span></a></li>
										<li><a href="<?= Url::to(['/service']) ?>"><i class="icon-googleplus5"></i> <span>Сервисы</span></a></li>
									</ul>
								</li>
								<?php if(Yii::$app->user->identity->role_id == '1'):?>
									<li>
										<a href="#"><i class="icon-info3"></i> <span>Инфо</span></a>
										<ul>
											<li><a href="<?= Url::to(['/info-embassy']) ?>"><i class="icon-stack2"></i> <span>Ппосольство</span></a></li>
											<li><a href="<?= Url::to(['/info-uzb']) ?>"><i class="icon-stack2"></i> <span>Узбекистан</span></a></li>
											<li><a href="<?= Url::to(['/book']) ?>"><i class="icon-stack2"></i> <span>Книга</span></a></li>
											<li><a href="<?= Url::to(['/quotation']) ?>"><i class="icon-stack2"></i> <span>Процитируем</span></a></li>
										</ul>
									</li>
								<?php endif;?>
								<li>
									<a href="#"><i class="icon-image3"></i> <span>Галерея</span></a>
									<ul>
										<li><a href="<?= Url::to(['/photo']) ?>"><i class="icon-camera"></i> <span>Фото</span></a></li>
										<li><a href="<?= Url::to(['/video']) ?>"><i class="icon-clapboard-play"></i> <span>Видео</span></a></li>
									</ul>
								</li>
								<?php if(Yii::$app->user->identity->role_id == '1'):?>
									<li>
										<a href="#"><i class="icon-stack"></i> <span>Конституция</span></a>
										<ul>
											<li><a href="<?= Url::to(['/section']) ?>"><i class="icon-stack2"></i> <span>РАЗДЕЛ</span></a></li>
											<li><a href="<?= Url::to(['/chapter']) ?>"><i class="icon-stack2"></i> <span>Глава</span></a></li>
											<li><a href="<?= Url::to(['/article']) ?>"><i class="icon-stack2"></i> <span>Статья</span></a></li>
										</ul>
									</li>
								<?php endif;?>
								<li>
										<?= yii\helpers\Html::a('Chiqish', ['/site/logout'], ['class'=>'btn btn-danger btn-flat', 'data-method'=>'post']) ?>

								</li>


							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
