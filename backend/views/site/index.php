<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<!-- Questions area -->
                    <div class="row">
                        <div class="col-lg-12">

                            <!-- Questions list -->
                            <?php $i = 1;?>
                            <?php foreach ($embassy as $key => $info):?>
                                <?php $i++;?>
                            <div class="panel-group panel-group-control panel-group-control-right">
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h6 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" href="#question<?= $i;?>">
                                                 <?= $info['name_uz'];?>
                                            </a>
                                        </h6>
                                    </div>

                                    <div id="question<?= $i;?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <?= $info['text_uz'];?>
                                        </div>

                                        <div class="panel-footer panel-footer-transparent">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>

                        </div>
                    </div>
                            <!-- /questions list -->
