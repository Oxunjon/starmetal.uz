<?php

namespace backend\models;
use backend\models\Energy;
use backend\models\Car;

use Yii;

/**
 * This is the model class for table "carenergy".
 *
 * @property int $id
 * @property int $car_id
 * @property int $energy_id
 */
class Carenergy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carenergy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'energy_id'], 'required'],
            [['car_id', 'energy_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_id' => 'Car ID',
            'energy_id' => 'Energy ID',
        ];
    }

    public function getCar(){
        return $this->hasOne(Car::className(), ['id'=>'car_id']);
    }
    public function getEnergy(){
        return $this->hasOne(Energy::className(), ['id'=>'energy_id']);
    }
}
