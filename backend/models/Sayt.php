<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sayt".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_uz
 * @property string $address
 * @property string $text_ru
 * @property string $text_uz
 * @property string $fax
 * @property string $email_em
 * @property string $link
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $logo
 */
class Sayt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sayt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['id', 'logo'], 'required'],
            [['id'], 'integer'],
            [['text_ru', 'text_uz', 'logo'], 'string'],
            [['name_ru', 'name_uz', 'address', 'link', 'facebook', 'twitter', 'instagram'], 'string', 'max' => 500],
            [['fax', 'email_em'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Name Ru',
            'name_uz' => 'Name Uz',
            'address' => 'Address',
            'text_ru' => 'Text Ru',
            'text_uz' => 'Text Uz',
            'fax' => 'Telephone',
            'email_em' => 'Email Em',
            'link' => 'Link',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'logo' => 'Logo',
        ];
    }
}
