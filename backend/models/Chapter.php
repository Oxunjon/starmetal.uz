<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "chapter".
 *
 * @property integer $id
 * @property integer $section_id
 * @property string $uz_chapter
 * @property string $ru_chapter
 * @property string $en_chapter
 * @property string $x_chapter
 */
class Chapter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chapter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id', 'uz_chapter', 'ru_chapter', 'en_chapter', 'x_chapter'], 'required'],
            [['section_id'], 'integer'],
            [['uz_chapter', 'ru_chapter', 'en_chapter', 'x_chapter'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'section_id' => Yii::t('app', 'Section ID'),
            'uz_chapter' => Yii::t('app', 'Uz Chapter'),
            'ru_chapter' => Yii::t('app', 'Ru Chapter'),
            'en_chapter' => Yii::t('app', 'En Chapter'),
            'x_chapter' => Yii::t('app', 'X Chapter'),
        ];
    }
}
